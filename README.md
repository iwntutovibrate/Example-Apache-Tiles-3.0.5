--------------------------------------------------------------------------------

1. information


GitLab Project Name : Example-Apache-Tiles-3.0.5

Spring Project Name : tiles


Project Specification
        
    Java JDK Version : 1.8
    
    WAS : Apache.tomcat.8.0.52
    
    Apache Tiles Version : 3.0.5


@ upload : 2018.07.16

--------------------------------------------------------------------------------

2. Reference

    #.1 pom.xml
    
        Add Maven dependency ( or .jar file )
    
    #.2 servlet.context.xml
    
        Add tilesViewResolver, tilesConfigurer
        
    #.3 tiles.xml
    
        Add tiles-definitions
        
    